11/19/2020 04:48:41 PM: [ COMMAND: retrieval_train.py --batch-size 256 --bert-dim 300 --cuda --dataset-name empchat --dict-max-words 250000 --display-iter 100 --embeddings None --empchat-folder ./empatheticdialogues --max-hist-len 4 --model bert --model-dir ./evaluation --model-name model --optimizer adamax --pretrained ./Model/model.mdl --reactonly --fasttext 1 --fasttext-path ./pretrained/ --fasttext-type emo ]
11/19/2020 04:48:41 PM: [ ---------------------------------------------------------------------------------------------------- ]
11/19/2020 04:48:41 PM: [ CONFIG:
{
    "batch_size": 256,
    "bert_add_transformer_layer": false,
    "bert_dim": 300,
    "cuda": true,
    "dailydialog_folder": null,
    "dataset_name": "empchat",
    "dict_max_words": 250000,
    "display_iter": 100,
    "embeddings": "None",
    "embeddings_size": 300,
    "empchat_folder": "./empatheticdialogues",
    "epoch_start": 0,
    "fasttext": 1,
    "fasttext_path": "./pretrained/",
    "fasttext_type": "emo",
    "hits_at_nb_cands": 100,
    "learn_embeddings": false,
    "learning_rate": null,
    "load_checkpoint": null,
    "log_file": "./evaluation\\model.txt",
    "max_hist_len": 4,
    "max_sent_len": 100,
    "model": "bert",
    "model_dir": "./evaluation",
    "model_file": "./evaluation\\model.mdl",
    "model_name": "model",
    "n_layers": 6,
    "no_shuffle": false,
    "normalize_emb": false,
    "normalize_sent_emb": false,
    "num_epochs": 1000,
    "optimizer": "adamax",
    "pretrained": "./Model/model.mdl",
    "random_seed": 92179,
    "reactonly": true,
    "reddit_folder": null,
    "rm_long_contexts": false,
    "rm_long_sent": false,
    "stop_crit_num_epochs": -1,
    "transformer_dim": 512,
    "transformer_dropout": 0,
    "transformer_n_heads": 8
} ]
11/19/2020 04:48:41 PM: [ Loading model ./Model/model.mdl ]
11/19/2020 04:49:29 PM: [ COMMAND: retrieval_train.py --batch-size 256 --bert-dim 300 --cuda --dataset-name empchat --dict-max-words 250000 --display-iter 100 --embeddings None --empchat-folder ./empatheticdialogues --max-hist-len 4 --model bert --model-dir ./evaluation --model-name model --optimizer adamax --pretrained ./Models/model.mdl --reactonly --fasttext 1 --fasttext-path ./pretrained/ --fasttext-type emo ]
11/19/2020 04:49:29 PM: [ ---------------------------------------------------------------------------------------------------- ]
11/19/2020 04:49:29 PM: [ CONFIG:
{
    "batch_size": 256,
    "bert_add_transformer_layer": false,
    "bert_dim": 300,
    "cuda": true,
    "dailydialog_folder": null,
    "dataset_name": "empchat",
    "dict_max_words": 250000,
    "display_iter": 100,
    "embeddings": "None",
    "embeddings_size": 300,
    "empchat_folder": "./empatheticdialogues",
    "epoch_start": 0,
    "fasttext": 1,
    "fasttext_path": "./pretrained/",
    "fasttext_type": "emo",
    "hits_at_nb_cands": 100,
    "learn_embeddings": false,
    "learning_rate": null,
    "load_checkpoint": null,
    "log_file": "./evaluation\\model.txt",
    "max_hist_len": 4,
    "max_sent_len": 100,
    "model": "bert",
    "model_dir": "./evaluation",
    "model_file": "./evaluation\\model.mdl",
    "model_name": "model",
    "n_layers": 6,
    "no_shuffle": false,
    "normalize_emb": false,
    "normalize_sent_emb": false,
    "num_epochs": 1000,
    "optimizer": "adamax",
    "pretrained": "./Models/model.mdl",
    "random_seed": 92179,
    "reactonly": true,
    "reddit_folder": null,
    "rm_long_contexts": false,
    "rm_long_sent": false,
    "stop_crit_num_epochs": -1,
    "transformer_dim": 512,
    "transformer_dropout": 0,
    "transformer_n_heads": 8
} ]
11/19/2020 04:49:29 PM: [ Loading model ./Models/model.mdl ]
11/19/2020 04:49:35 PM: [ loading archive file https://s3.amazonaws.com/models.huggingface.co/bert/bert-base-cased.tar.gz from cache at C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c ]
11/19/2020 04:49:35 PM: [ extracting archive file C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c to temp dir C:\Users\chuachen\AppData\Local\Temp\tmpxs1oljpl ]
11/19/2020 04:49:41 PM: [ Model config {
  "attention_probs_dropout_prob": 0.1,
  "hidden_act": "gelu",
  "hidden_dropout_prob": 0.1,
  "hidden_size": 768,
  "initializer_range": 0.02,
  "intermediate_size": 3072,
  "max_position_embeddings": 512,
  "num_attention_heads": 12,
  "num_hidden_layers": 12,
  "type_vocab_size": 2,
  "vocab_size": 28996
}
 ]
11/19/2020 04:49:45 PM: [ loading archive file https://s3.amazonaws.com/models.huggingface.co/bert/bert-base-cased.tar.gz from cache at C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c ]
11/19/2020 04:49:45 PM: [ extracting archive file C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c to temp dir C:\Users\chuachen\AppData\Local\Temp\tmprymw2tdu ]
11/19/2020 04:49:51 PM: [ Model config {
  "attention_probs_dropout_prob": 0.1,
  "hidden_act": "gelu",
  "hidden_dropout_prob": 0.1,
  "hidden_size": 768,
  "initializer_range": 0.02,
  "intermediate_size": 3072,
  "max_position_embeddings": 512,
  "num_attention_heads": 12,
  "num_hidden_layers": 12,
  "type_vocab_size": 2,
  "vocab_size": 28996
}
 ]
11/19/2020 04:49:55 PM: [ loading archive file https://s3.amazonaws.com/models.huggingface.co/bert/bert-base-cased.tar.gz from cache at C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c ]
11/19/2020 04:49:55 PM: [ extracting archive file C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c to temp dir C:\Users\chuachen\AppData\Local\Temp\tmp42fnoab7 ]
11/19/2020 04:50:01 PM: [ Model config {
  "attention_probs_dropout_prob": 0.1,
  "hidden_act": "gelu",
  "hidden_dropout_prob": 0.1,
  "hidden_size": 768,
  "initializer_range": 0.02,
  "intermediate_size": 3072,
  "max_position_embeddings": 512,
  "num_attention_heads": 12,
  "num_hidden_layers": 12,
  "type_vocab_size": 2,
  "vocab_size": 28996
}
 ]
11/19/2020 04:58:52 PM: [ COMMAND: retrieval_train.py --batch-size 256 --bert-dim 300 --cuda --dataset-name empchat --dict-max-words 250000 --display-iter 100 --embeddings None --empchat-folder ./empatheticdialogues --max-hist-len 4 --model bert --model-dir ./evaluation --model-name model --optimizer adamax --pretrained ./Models/model.mdl --reactonly --fasttext 1 --fasttext-path ./pretrained/ --fasttext-type emo ]
11/19/2020 04:58:52 PM: [ ---------------------------------------------------------------------------------------------------- ]
11/19/2020 04:58:52 PM: [ CONFIG:
{
    "batch_size": 256,
    "bert_add_transformer_layer": false,
    "bert_dim": 300,
    "cuda": true,
    "dailydialog_folder": null,
    "dataset_name": "empchat",
    "dict_max_words": 250000,
    "display_iter": 100,
    "embeddings": "None",
    "embeddings_size": 300,
    "empchat_folder": "./empatheticdialogues",
    "epoch_start": 0,
    "fasttext": 1,
    "fasttext_path": "./pretrained/",
    "fasttext_type": "emo",
    "hits_at_nb_cands": 100,
    "learn_embeddings": false,
    "learning_rate": null,
    "load_checkpoint": null,
    "log_file": "./evaluation\\model.txt",
    "max_hist_len": 4,
    "max_sent_len": 100,
    "model": "bert",
    "model_dir": "./evaluation",
    "model_file": "./evaluation\\model.mdl",
    "model_name": "model",
    "n_layers": 6,
    "no_shuffle": false,
    "normalize_emb": false,
    "normalize_sent_emb": false,
    "num_epochs": 1000,
    "optimizer": "adamax",
    "pretrained": "./Models/model.mdl",
    "random_seed": 92179,
    "reactonly": true,
    "reddit_folder": null,
    "rm_long_contexts": false,
    "rm_long_sent": false,
    "stop_crit_num_epochs": -1,
    "transformer_dim": 512,
    "transformer_dropout": 0,
    "transformer_n_heads": 8
} ]
11/19/2020 04:58:52 PM: [ Loading model ./Models/model.mdl ]
11/19/2020 04:59:30 PM: [ COMMAND: retrieval_train.py --batch-size 256 --bert-dim 300 --cuda --dataset-name empchat --dict-max-words 250000 --display-iter 100 --embeddings None --empchat-folder ./empatheticdialogues --max-hist-len 4 --model bert --model-dir ./evaluation --model-name model --optimizer adamax --pretrained ./Models/model.mdl --reactonly --fasttext 0 --fasttext-path ./pretrained/ --fasttext-type emo ]
11/19/2020 04:59:30 PM: [ ---------------------------------------------------------------------------------------------------- ]
11/19/2020 04:59:30 PM: [ CONFIG:
{
    "batch_size": 256,
    "bert_add_transformer_layer": false,
    "bert_dim": 300,
    "cuda": true,
    "dailydialog_folder": null,
    "dataset_name": "empchat",
    "dict_max_words": 250000,
    "display_iter": 100,
    "embeddings": "None",
    "embeddings_size": 300,
    "empchat_folder": "./empatheticdialogues",
    "epoch_start": 0,
    "fasttext": 0,
    "fasttext_path": "./pretrained/",
    "fasttext_type": "emo",
    "hits_at_nb_cands": 100,
    "learn_embeddings": false,
    "learning_rate": null,
    "load_checkpoint": null,
    "log_file": "./evaluation\\model.txt",
    "max_hist_len": 4,
    "max_sent_len": 100,
    "model": "bert",
    "model_dir": "./evaluation",
    "model_file": "./evaluation\\model.mdl",
    "model_name": "model",
    "n_layers": 6,
    "no_shuffle": false,
    "normalize_emb": false,
    "normalize_sent_emb": false,
    "num_epochs": 1000,
    "optimizer": "adamax",
    "pretrained": "./Models/model.mdl",
    "random_seed": 92179,
    "reactonly": true,
    "reddit_folder": null,
    "rm_long_contexts": false,
    "rm_long_sent": false,
    "stop_crit_num_epochs": -1,
    "transformer_dim": 512,
    "transformer_dropout": 0,
    "transformer_n_heads": 8
} ]
11/19/2020 04:59:30 PM: [ Loading model ./Models/model.mdl ]
11/19/2020 04:59:34 PM: [ loading archive file https://s3.amazonaws.com/models.huggingface.co/bert/bert-base-cased.tar.gz from cache at C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c ]
11/19/2020 04:59:34 PM: [ extracting archive file C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c to temp dir C:\Users\chuachen\AppData\Local\Temp\tmpv_h8x8fa ]
11/19/2020 04:59:40 PM: [ Model config {
  "attention_probs_dropout_prob": 0.1,
  "hidden_act": "gelu",
  "hidden_dropout_prob": 0.1,
  "hidden_size": 768,
  "initializer_range": 0.02,
  "intermediate_size": 3072,
  "max_position_embeddings": 512,
  "num_attention_heads": 12,
  "num_hidden_layers": 12,
  "type_vocab_size": 2,
  "vocab_size": 28996
}
 ]
11/19/2020 04:59:44 PM: [ loading archive file https://s3.amazonaws.com/models.huggingface.co/bert/bert-base-cased.tar.gz from cache at C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c ]
11/19/2020 04:59:44 PM: [ extracting archive file C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c to temp dir C:\Users\chuachen\AppData\Local\Temp\tmpw3k4a27y ]
11/19/2020 04:59:50 PM: [ Model config {
  "attention_probs_dropout_prob": 0.1,
  "hidden_act": "gelu",
  "hidden_dropout_prob": 0.1,
  "hidden_size": 768,
  "initializer_range": 0.02,
  "intermediate_size": 3072,
  "max_position_embeddings": 512,
  "num_attention_heads": 12,
  "num_hidden_layers": 12,
  "type_vocab_size": 2,
  "vocab_size": 28996
}
 ]
11/19/2020 04:59:54 PM: [ loading archive file https://s3.amazonaws.com/models.huggingface.co/bert/bert-base-cased.tar.gz from cache at C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c ]
11/19/2020 04:59:54 PM: [ extracting archive file C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c to temp dir C:\Users\chuachen\AppData\Local\Temp\tmpnn9pfj5z ]
11/19/2020 05:00:00 PM: [ Model config {
  "attention_probs_dropout_prob": 0.1,
  "hidden_act": "gelu",
  "hidden_dropout_prob": 0.1,
  "hidden_size": 768,
  "initializer_range": 0.02,
  "intermediate_size": 3072,
  "max_position_embeddings": 512,
  "num_attention_heads": 12,
  "num_hidden_layers": 12,
  "type_vocab_size": 2,
  "vocab_size": 28996
}
 ]
11/19/2020 05:07:00 PM: [ COMMAND: retrieval_train.py --batch-size 512 --cuda --dataset-name empchat --dict-max-words 250000 --display-iter 250 --empchat-folder ./empatheticdialogues --max-hist-len 4 --model transformer --model-dir ./evaluation --model-name model --n-layers 4 --optimizer adamax --pretrained ./Models/model.mdl --reactonly --transformer-dim 300 --transformer-n-heads 6 ]
11/19/2020 05:07:00 PM: [ ---------------------------------------------------------------------------------------------------- ]
11/19/2020 05:07:00 PM: [ CONFIG:
{
    "batch_size": 512,
    "bert_add_transformer_layer": false,
    "bert_dim": 512,
    "cuda": true,
    "dailydialog_folder": null,
    "dataset_name": "empchat",
    "dict_max_words": 250000,
    "display_iter": 250,
    "embeddings": null,
    "embeddings_size": 300,
    "empchat_folder": "./empatheticdialogues",
    "epoch_start": 0,
    "fasttext": null,
    "fasttext_path": null,
    "fasttext_type": null,
    "hits_at_nb_cands": 100,
    "learn_embeddings": false,
    "learning_rate": null,
    "load_checkpoint": null,
    "log_file": "./evaluation\\model.txt",
    "max_hist_len": 4,
    "max_sent_len": 100,
    "model": "transformer",
    "model_dir": "./evaluation",
    "model_file": "./evaluation\\model.mdl",
    "model_name": "model",
    "n_layers": 4,
    "no_shuffle": false,
    "normalize_emb": false,
    "normalize_sent_emb": false,
    "num_epochs": 1000,
    "optimizer": "adamax",
    "pretrained": "./Models/model.mdl",
    "random_seed": 92179,
    "reactonly": true,
    "reddit_folder": null,
    "rm_long_contexts": false,
    "rm_long_sent": false,
    "stop_crit_num_epochs": -1,
    "transformer_dim": 300,
    "transformer_dropout": 0,
    "transformer_n_heads": 6
} ]
11/19/2020 05:07:00 PM: [ Loading model ./Models/model.mdl ]
11/19/2020 05:07:04 PM: [ loading archive file https://s3.amazonaws.com/models.huggingface.co/bert/bert-base-cased.tar.gz from cache at C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c ]
11/19/2020 05:07:04 PM: [ extracting archive file C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c to temp dir C:\Users\chuachen\AppData\Local\Temp\tmp6vq50t50 ]
11/19/2020 05:07:10 PM: [ Model config {
  "attention_probs_dropout_prob": 0.1,
  "hidden_act": "gelu",
  "hidden_dropout_prob": 0.1,
  "hidden_size": 768,
  "initializer_range": 0.02,
  "intermediate_size": 3072,
  "max_position_embeddings": 512,
  "num_attention_heads": 12,
  "num_hidden_layers": 12,
  "type_vocab_size": 2,
  "vocab_size": 28996
}
 ]
11/19/2020 05:07:14 PM: [ loading archive file https://s3.amazonaws.com/models.huggingface.co/bert/bert-base-cased.tar.gz from cache at C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c ]
11/19/2020 05:07:14 PM: [ extracting archive file C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c to temp dir C:\Users\chuachen\AppData\Local\Temp\tmp58gx_p13 ]
11/19/2020 05:07:20 PM: [ Model config {
  "attention_probs_dropout_prob": 0.1,
  "hidden_act": "gelu",
  "hidden_dropout_prob": 0.1,
  "hidden_size": 768,
  "initializer_range": 0.02,
  "intermediate_size": 3072,
  "max_position_embeddings": 512,
  "num_attention_heads": 12,
  "num_hidden_layers": 12,
  "type_vocab_size": 2,
  "vocab_size": 28996
}
 ]
11/19/2020 05:07:24 PM: [ loading archive file https://s3.amazonaws.com/models.huggingface.co/bert/bert-base-cased.tar.gz from cache at C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c ]
11/19/2020 05:07:24 PM: [ extracting archive file C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c to temp dir C:\Users\chuachen\AppData\Local\Temp\tmp76fpofpe ]
11/19/2020 05:07:30 PM: [ Model config {
  "attention_probs_dropout_prob": 0.1,
  "hidden_act": "gelu",
  "hidden_dropout_prob": 0.1,
  "hidden_size": 768,
  "initializer_range": 0.02,
  "intermediate_size": 3072,
  "max_position_embeddings": 512,
  "num_attention_heads": 12,
  "num_hidden_layers": 12,
  "type_vocab_size": 2,
  "vocab_size": 28996
}
 ]
11/19/2020 09:17:34 PM: [ COMMAND: retrieval_train.py --batch-size 256 --bert-dim 300 --cuda --dataset-name empchat --dict-max-words 250000 --display-iter 100 --embeddings None --empchat-folder ./empatheticdialogues --max-hist-len 4 --model bert --model-dir ./evaluation --model-name model --optimizer adamax --pretrained ./Models/model.mdl --reactonly ]
11/19/2020 09:17:34 PM: [ ---------------------------------------------------------------------------------------------------- ]
11/19/2020 09:17:34 PM: [ CONFIG:
{
    "batch_size": 256,
    "bert_add_transformer_layer": false,
    "bert_dim": 300,
    "cuda": true,
    "dailydialog_folder": null,
    "dataset_name": "empchat",
    "dict_max_words": 250000,
    "display_iter": 100,
    "embeddings": "None",
    "embeddings_size": 300,
    "empchat_folder": "./empatheticdialogues",
    "epoch_start": 0,
    "fasttext": null,
    "fasttext_path": null,
    "fasttext_type": null,
    "hits_at_nb_cands": 100,
    "learn_embeddings": false,
    "learning_rate": null,
    "load_checkpoint": null,
    "log_file": "./evaluation\\model.txt",
    "max_hist_len": 4,
    "max_sent_len": 100,
    "model": "bert",
    "model_dir": "./evaluation",
    "model_file": "./evaluation\\model.mdl",
    "model_name": "model",
    "n_layers": 6,
    "no_shuffle": false,
    "normalize_emb": false,
    "normalize_sent_emb": false,
    "num_epochs": 1000,
    "optimizer": "adamax",
    "pretrained": "./Models/model.mdl",
    "random_seed": 92179,
    "reactonly": true,
    "reddit_folder": null,
    "rm_long_contexts": false,
    "rm_long_sent": false,
    "stop_crit_num_epochs": -1,
    "transformer_dim": 512,
    "transformer_dropout": 0,
    "transformer_n_heads": 8
} ]
11/19/2020 09:17:34 PM: [ Loading model ./Models/model.mdl ]
11/19/2020 09:17:38 PM: [ loading archive file https://s3.amazonaws.com/models.huggingface.co/bert/bert-base-cased.tar.gz from cache at C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c ]
11/19/2020 09:17:38 PM: [ extracting archive file C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c to temp dir C:\Users\chuachen\AppData\Local\Temp\tmp6ni2no0l ]
11/19/2020 09:17:44 PM: [ Model config {
  "attention_probs_dropout_prob": 0.1,
  "hidden_act": "gelu",
  "hidden_dropout_prob": 0.1,
  "hidden_size": 768,
  "initializer_range": 0.02,
  "intermediate_size": 3072,
  "max_position_embeddings": 512,
  "num_attention_heads": 12,
  "num_hidden_layers": 12,
  "type_vocab_size": 2,
  "vocab_size": 28996
}
 ]
11/19/2020 09:17:48 PM: [ loading archive file https://s3.amazonaws.com/models.huggingface.co/bert/bert-base-cased.tar.gz from cache at C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c ]
11/19/2020 09:17:48 PM: [ extracting archive file C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c to temp dir C:\Users\chuachen\AppData\Local\Temp\tmpmc4hi9k3 ]
11/19/2020 09:17:54 PM: [ Model config {
  "attention_probs_dropout_prob": 0.1,
  "hidden_act": "gelu",
  "hidden_dropout_prob": 0.1,
  "hidden_size": 768,
  "initializer_range": 0.02,
  "intermediate_size": 3072,
  "max_position_embeddings": 512,
  "num_attention_heads": 12,
  "num_hidden_layers": 12,
  "type_vocab_size": 2,
  "vocab_size": 28996
}
 ]
11/19/2020 09:17:58 PM: [ loading archive file https://s3.amazonaws.com/models.huggingface.co/bert/bert-base-cased.tar.gz from cache at C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c ]
11/19/2020 09:17:58 PM: [ extracting archive file C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c to temp dir C:\Users\chuachen\AppData\Local\Temp\tmptjd51xcp ]
11/19/2020 09:18:03 PM: [ Model config {
  "attention_probs_dropout_prob": 0.1,
  "hidden_act": "gelu",
  "hidden_dropout_prob": 0.1,
  "hidden_size": 768,
  "initializer_range": 0.02,
  "intermediate_size": 3072,
  "max_position_embeddings": 512,
  "num_attention_heads": 12,
  "num_hidden_layers": 12,
  "type_vocab_size": 2,
  "vocab_size": 28996
}
 ]
11/20/2020 04:57:47 PM: [ COMMAND: retrieval_train.py --batch-size 256 --bert-dim 300 --cuda --dataset-name empchat --dict-max-words 250000 --display-iter 100 --embeddings None --empchat-folder ./empatheticdialogues --max-hist-len 4 --model bert --model-dir ./evaluation --model-name model --optimizer adamax --pretrained ./Models/model.mdl --reactonly --fasttext 0 --fasttext-path ./pretrained/ --fasttext-type emo ]
11/20/2020 04:57:47 PM: [ ---------------------------------------------------------------------------------------------------- ]
11/20/2020 04:57:47 PM: [ CONFIG:
{
    "batch_size": 256,
    "bert_add_transformer_layer": false,
    "bert_dim": 300,
    "cuda": true,
    "dailydialog_folder": null,
    "dataset_name": "empchat",
    "dict_max_words": 250000,
    "display_iter": 100,
    "embeddings": "None",
    "embeddings_size": 300,
    "empchat_folder": "./empatheticdialogues",
    "epoch_start": 0,
    "fasttext": 0,
    "fasttext_path": "./pretrained/",
    "fasttext_type": "emo",
    "hits_at_nb_cands": 100,
    "learn_embeddings": false,
    "learning_rate": null,
    "load_checkpoint": null,
    "log_file": "./evaluation\\model.txt",
    "max_hist_len": 4,
    "max_sent_len": 100,
    "model": "bert",
    "model_dir": "./evaluation",
    "model_file": "./evaluation\\model.mdl",
    "model_name": "model",
    "n_layers": 6,
    "no_shuffle": false,
    "normalize_emb": false,
    "normalize_sent_emb": false,
    "num_epochs": 1000,
    "optimizer": "adamax",
    "pretrained": "./Models/model.mdl",
    "random_seed": 92179,
    "reactonly": true,
    "reddit_folder": null,
    "rm_long_contexts": false,
    "rm_long_sent": false,
    "stop_crit_num_epochs": -1,
    "transformer_dim": 512,
    "transformer_dropout": 0,
    "transformer_n_heads": 8
} ]
11/20/2020 04:57:47 PM: [ Loading model ./Models/model.mdl ]
11/20/2020 04:57:56 PM: [ loading archive file https://s3.amazonaws.com/models.huggingface.co/bert/bert-base-cased.tar.gz from cache at C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c ]
11/20/2020 04:57:56 PM: [ extracting archive file C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c to temp dir C:\Users\chuachen\AppData\Local\Temp\tmp0qui12l2 ]
11/20/2020 04:58:02 PM: [ Model config {
  "attention_probs_dropout_prob": 0.1,
  "hidden_act": "gelu",
  "hidden_dropout_prob": 0.1,
  "hidden_size": 768,
  "initializer_range": 0.02,
  "intermediate_size": 3072,
  "max_position_embeddings": 512,
  "num_attention_heads": 12,
  "num_hidden_layers": 12,
  "type_vocab_size": 2,
  "vocab_size": 28996
}
 ]
11/20/2020 04:58:06 PM: [ loading archive file https://s3.amazonaws.com/models.huggingface.co/bert/bert-base-cased.tar.gz from cache at C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c ]
11/20/2020 04:58:06 PM: [ extracting archive file C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c to temp dir C:\Users\chuachen\AppData\Local\Temp\tmpij64tlb4 ]
11/20/2020 04:58:12 PM: [ Model config {
  "attention_probs_dropout_prob": 0.1,
  "hidden_act": "gelu",
  "hidden_dropout_prob": 0.1,
  "hidden_size": 768,
  "initializer_range": 0.02,
  "intermediate_size": 3072,
  "max_position_embeddings": 512,
  "num_attention_heads": 12,
  "num_hidden_layers": 12,
  "type_vocab_size": 2,
  "vocab_size": 28996
}
 ]
11/20/2020 04:58:16 PM: [ loading archive file https://s3.amazonaws.com/models.huggingface.co/bert/bert-base-cased.tar.gz from cache at C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c ]
11/20/2020 04:58:16 PM: [ extracting archive file C:\home\chuachen\.pytorch_pretrained_bert\a803ce83ca27fecf74c355673c434e51c265fb8a3e0e57ac62a80e38ba98d384.681017f415dfb33ec8d0e04fe51a619f3f01532ecea04edbfd48c5d160550d9c to temp dir C:\Users\chuachen\AppData\Local\Temp\tmp87rmyhwt ]
11/20/2020 04:58:22 PM: [ Model config {
  "attention_probs_dropout_prob": 0.1,
  "hidden_act": "gelu",
  "hidden_dropout_prob": 0.1,
  "hidden_size": 768,
  "initializer_range": 0.02,
  "intermediate_size": 3072,
  "max_position_embeddings": 512,
  "num_attention_heads": 12,
  "num_hidden_layers": 12,
  "type_vocab_size": 2,
  "vocab_size": 28996
}
 ]
